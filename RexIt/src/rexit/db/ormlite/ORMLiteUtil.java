
package rexit.db.ormlite;

import java.util.ArrayList;
import java.util.List;

import rexit.context.models.RexItAttraction;
import rexit.xml.simplexml.ResponseAttractions;
import rexit.xml.simplexml.SimpleXmlParser;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

/**
 * 
 * Class: ORMLiteUtil
 * 
 * Notes: Holds commonly used calls to the RexIt database.
 */
public class ORMLiteUtil {
	
	private final static String LOG_TAG = "ORMLiteUtil";
	
	/*
	 * Method: ormLiteCreateIfNotExists() 
	 * 
	 * Notes: Creates a new entry in the rexit_attraction table
	 * in the database 
	 */
	public static void ormLiteCreateIfNotExists(Context mContext, RexItAttraction attraction){
		Log.d(LOG_TAG, "ormLiteCreateIfNotExists()");
		
		// Get DAO
		final ORMLiteHelper helper = OpenHelperManager.getHelper(mContext, ORMLiteHelper.class);
		ORMLiteDao<RexItAttraction, Integer> attractionDao = helper.getORMLiteDao(RexItAttraction.class);
		
		// Save attraction to database
		if(attractionDao == null) return;
		attractionDao.createIfNotExists(attraction);
		OpenHelperManager.releaseHelper();
	}
	
	/*
	 * Method: ormLiteUpdateAttraction() 
	 * 
	 * Notes: Updates an entry in the rexit_attraction table
	 * in the database 
	 */
	public static void ormLiteUpdateAttraction(Context mContext, RexItAttraction attraction){
		Log.d(LOG_TAG, "ormLiteUpdateAttraction()");

		// Get DAO
		final ORMLiteHelper helper = OpenHelperManager.getHelper(mContext, ORMLiteHelper.class);
		ORMLiteDao<RexItAttraction, Integer> attractionDao = helper.getORMLiteDao(RexItAttraction.class);
		
		// Update attraction
		attractionDao.update(attraction);
		OpenHelperManager.releaseHelper();
	}
	
	/*
	 * Method: ormLiteQueryForAttractionId() 
	 * 
	 * Notes: Retrieves an entry from the rexit_attraction table
	 * in the database where the id = id 
	 */
	public static RexItAttraction ormLiteQueryForAttractionId(Context mContext, int id){
		Log.d(LOG_TAG, "ormLiteQueryForAttractionId()");

		// Get DAO
		final ORMLiteHelper helper = OpenHelperManager.getHelper(mContext, ORMLiteHelper.class);
		ORMLiteDao<RexItAttraction, Integer> attractionDao = helper.getORMLiteDao(RexItAttraction.class);
		
		// Retrieve attraction from database
		RexItAttraction attraction = attractionDao.queryForId(id);
		OpenHelperManager.releaseHelper();
		return attraction;
	}
	
	/*
	 * Method: ormLiteQueryForAllAttraction() 
	 * 
	 * Notes: Retrieves all entries from the rexit_attraction table
	 * in the database. Returned as an ArrayList<Attraction>
	 */
	public static ArrayList<RexItAttraction> ormLiteQueryForAllAttraction(Context mContext){
		Log.d(LOG_TAG, "ormLiteQueryForAllAttraction()");
		
		// Get DAO
		final ORMLiteHelper helper = OpenHelperManager.getHelper(mContext, ORMLiteHelper.class);
		ORMLiteDao<RexItAttraction, Integer> attractionDao = helper.getORMLiteDao(RexItAttraction.class);
		
		// Retrieve all attractions from database
		List<RexItAttraction> attractions = attractionDao.queryForAll();
		OpenHelperManager.releaseHelper();
		return new ArrayList<RexItAttraction>(attractions);
	}
	
	public static ArrayList<RexItAttraction> ormLiteQueryForMatchingAttractionsRaining(Context mContext, String time){
		Log.d(LOG_TAG, "ormLiteQueryForMatchingAttractions()");
		
		// Get DAO
		final ORMLiteHelper helper = OpenHelperManager.getHelper(mContext, ORMLiteHelper.class);
		ORMLiteDao<RexItAttraction, Integer> attractionDao = helper.getORMLiteDao(RexItAttraction.class);
		
		try{
			// Retrieve all attractions from database where weather == current weather && currTime between start and end time
			QueryBuilder<RexItAttraction, Integer> queryBuilder = attractionDao.queryBuilder();
			queryBuilder.where().eq(RexItAttraction.RAIN, "true").and().gt(RexItAttraction.CLOSE, time).and().le(RexItAttraction.OPEN, time);
			PreparedQuery<RexItAttraction> preparedQuery = queryBuilder.prepare();
			List<RexItAttraction> attractions = attractionDao.query(preparedQuery);		
			OpenHelperManager.releaseHelper();
			return new ArrayList<RexItAttraction>(attractions);
		} catch (Exception ex){
			return null;
		}
	}
	
	public static ArrayList<RexItAttraction> ormLiteQueryForMatchingAttractionsSunny(Context mContext, String time){
		Log.d(LOG_TAG, "ormLiteQueryForMatchingAttractions()");
		
		// Get DAO
		final ORMLiteHelper helper = OpenHelperManager.getHelper(mContext, ORMLiteHelper.class);
		ORMLiteDao<RexItAttraction, Integer> attractionDao = helper.getORMLiteDao(RexItAttraction.class);
		
		try{
			// Retrieve all attractions from database where weather == current weather && currTime between start and end time
			QueryBuilder<RexItAttraction, Integer> queryBuilder = attractionDao.queryBuilder();
			queryBuilder.where().gt(RexItAttraction.CLOSE, time).and().le(RexItAttraction.OPEN, time);
			PreparedQuery<RexItAttraction> preparedQuery = queryBuilder.prepare();
			List<RexItAttraction> attractions = attractionDao.query(preparedQuery);		
			OpenHelperManager.releaseHelper();
			return new ArrayList<RexItAttraction>(attractions);
		} catch (Exception ex){
			return null;
		}
	}
	
    /*
	 * Method: generateDummyData() 
	 * 
	 * Notes: Load dummy data from res/values/array and store 
	 * in the 'RexIt' database. Only insert into the database
	 * the first time the application is opened.
	 */
	public static void ormLiteGenerateDummyData(Context context){
		Log.d(LOG_TAG, "generateDummyData()");
    	
    	SharedPreferences prefs = context.getSharedPreferences("RexIt", Activity.MODE_PRIVATE); 
    	if(prefs.getBoolean("onFirstInstance", true)){
    		
    		ResponseAttractions response = SimpleXmlParser.serializeXML(context);
    		if(response.getAttractions() == null || response.getAttractions().isEmpty()) return;
    		
    		for(RexItAttraction attr : response.getAttractions()){
    			ORMLiteUtil.ormLiteCreateIfNotExists(context, attr);
    		}
    	}
    	SharedPreferences.Editor editor = context.getSharedPreferences("RexIt", Activity.MODE_PRIVATE).edit();    	
    	editor.putBoolean("onFirstInstance", false); editor.commit();
    }
}
