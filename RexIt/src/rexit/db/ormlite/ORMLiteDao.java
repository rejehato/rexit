
package rexit.db.ormlite;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;

/**
 * 
 * Class: ORMLiteDao
 * 
 * Notes: Database Access Object (DAO). Used to create a DAO for 
 * a given class. Relates to the RexIt database. For now it will be
 * used only for the Attraction class. However note that generic types 
 * are used. This allows for multiple DAO types to be handled by one class.
 * T = class (i.e. Attraction) ID = type of id (i.e. Integer)
 */
@SuppressWarnings("unused")
public class ORMLiteDao<T, ID> extends RuntimeExceptionDao<T, ID> {
	
	private Dao<T, ID> dao;
	
	public ORMLiteDao(Dao<T, ID> dao) {
		super(dao);
		this.dao = dao;
	}
}
