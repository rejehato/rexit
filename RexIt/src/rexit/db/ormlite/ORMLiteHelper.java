
package rexit.db.ormlite;

import java.sql.SQLException;

import rexit.context.models.RexItAttraction;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * 
 * Class: ORMLiteHelper
 * 
 * Notes: Handles the creation and updating of the database.
 */
public class ORMLiteHelper extends OrmLiteSqliteOpenHelper {

	private static final String DATABASE_NAME 		= "RexIt.db";
	private static final int 	DATABASE_VERSION 	= 1;

	public ORMLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/*
	 * Method: onCreate() 
	 * 
	 * Notes: Creates an empty table named "rexit_attractions"
	 * including rows relating to attributes in the Attraction class.
	 */
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
			Log.i(ORMLiteHelper.class.getName(), "onCreate()");
			TableUtils.createTable(connectionSource, RexItAttraction.class);;
		} catch (SQLException e) {
			Log.e(ORMLiteHelper.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}
	}

	/*
	 * Method: onUpgrade() 
	 * 
	 * Notes: 
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		super.onUpgrade(getWritableDatabase(), oldVersion, newVersion);
	}

	/*
	 * Method: getORMLiteDao() 
	 * 
	 * Notes: Returns the DAO for a table. Note that generic types are used.
	 * This allows for multiple DAO types to be handled by one method.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <D extends ORMLiteDao<?, ?>, T> D getORMLiteDao(Class<T> clazz) {
		try {
			Dao<T, ?> dao = getDao(clazz);
			D castDao = (D) new ORMLiteDao(dao);
			if (!dao.isTableExists()) {
				TableUtils.createTable(getConnectionSource(), clazz);
			}
			return castDao;
		} catch (SQLException e) {
			throw new RuntimeException("Could not create RexItDao for class " + clazz, e);
		}
	}

	@Override
	public void close() {
		super.close();
	}

}
