
package rexit.ui.fragments;

import java.util.ArrayList;

import rexit.api.applications.AttractionApplication;
import rexit.context.models.RexItAttraction;
import rexit.ui.activities.AttractionActivity;
import rexit.util.adapters.AttractionListAdapter;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class FragmentAttractionsList extends ListFragment {
	
	private static final String					LOG_TAG					= "FragmentAttractionsList";
	private static ArrayList<RexItAttraction>	docs					= new ArrayList<RexItAttraction>();

	private AttractionListAdapter				listAdapter;

	public FragmentAttractionsList() {
	}

	@Override
	public void onAttach(Activity activity) {
		Log.d(LOG_TAG, "onAttach()");
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(LOG_TAG, "onCreate()");
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d(LOG_TAG, "onActivityCreated()");
		super.onActivityCreated(savedInstanceState);
		listAdapter = new AttractionListAdapter(getActivity(), docs);
		getListView().setAdapter(listAdapter);
		getListView().setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos,
                    long id) {
            	AttractionApplication.setAttraction(docs.get(pos));
        		Intent i = new Intent(getActivity(), AttractionActivity.class);
        		startActivity(i);
            }
        });
	}
	
	public void notifyArticleListChanged(){
		listAdapter.notifyDataSetChanged();
	}

	public void setIsLoading(boolean is_loading) {
		setListShown(!is_loading);
	}


	public void update(ArrayList<RexItAttraction> retrievedArticles) {
		setIsLoading(false);
		docs.clear();
		docs.addAll(retrievedArticles);
		listAdapter.notifyDataSetChanged();
	}
}
