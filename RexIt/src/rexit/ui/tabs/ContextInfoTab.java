package rexit.ui.tabs;

import rexit.context.models.RexItDateTime;
import rexit.context.models.RexItWeather;
import rexit.util.util.SPHelper;
import nz.ac.waikato.cms.rexit.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ContextInfoTab extends Activity {
        
    private static String 			currTime;
    private static String[] 		location;
    private static RexItWeather 			weather;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tab_info);
					      
		// Get current time
		currTime = RexItDateTime.getRexItCurrentTime();
		  
		// Get current or last gps location
		location = SPHelper.getLocationSP(this);
		  
		// Get pre-downloaded weather data
		weather = SPHelper.getWeatherSP(this);
		  
		// Display context data
		displayContextData();
	}
	
	private void displayContextData(){    
		TextView textViewCity = (TextView) findViewById(R.id.textViewCity);
		TextView textViewTime = (TextView) findViewById(R.id.textViewTime);
		TextView textViewWeather = (TextView) findViewById(R.id.textViewWeather);
		       
		textViewCity.setText("Location: " + location[2]);
		textViewTime.setText("Time: " + currTime+"");
		textViewWeather.setText("Weather conditions: " + weather.getWeather());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.tab2, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
