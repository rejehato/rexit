package rexit.ui.tabs;

import java.util.ArrayList;
import java.util.Collections;

import nz.ac.waikato.cms.rexit.R;

import org.json.JSONObject;

import rexit.api.weatherapi.JSONParser;
import rexit.api.weatherapi.WeatherClientFragment;
import rexit.api.weatherapi.WeatherResponseListener;
import rexit.context.models.RexItAttraction;
import rexit.context.models.RexItDateTime;
import rexit.context.models.RexItLocation;
import rexit.context.models.RexItWeather;
import rexit.db.ormlite.ORMLiteUtil;
import rexit.ui.fragments.FragmentAttractionsList;
import rexit.util.util.CustomComparitor;
import rexit.util.util.SPHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

public class AttractionsTab extends Activity implements WeatherResponseListener{

	private final static String 	LOG_TAG 						= "AttractionsTab";
    private static final String		WEATHER_CLIENT_FRAG_TAG			= "WeatherClientFragment";
    private static final String		ATTRACTION_LIST_FRAG_TAG		= "FragmentAttractionsList";
    
    private FragmentManager 		fragmentManager;
    public WeatherClientFragment	weatherClientFragment;
	protected static Spinner 		mLevelSpinner;
	protected static Spinner 		mSemesterSpinner;
    
	protected int 					levelSelected 					= 0;
	private boolean 				isGPSReturned					= false;
	private boolean 				isNetworkReturned				= false;
	protected boolean				useContext						= true;
    private static String 			currTime;
    private static String[] 		location;
    private static RexItWeather		weather;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tab_attractions);
		
		Log.d(LOG_TAG, "onCreate()");
		fragmentManager = getFragmentManager();
		addNonUIFragments();
      
		// Get current time
	    currTime = RexItDateTime.getRexItCurrentTime();
		
		// Get current or last gps location
		retrieveGPSInfo();
      
	}
	
	public void onClickRefresh(View v){
		
	      // Get current time
	      currTime = RexItDateTime.getRexItCurrentTime();
	      
	      // Get current or last gps location
	      retrieveGPSInfo();
	}
	
   private void addNonUIFragments() {
        weatherClientFragment = (WeatherClientFragment) fragmentManager.findFragmentByTag(WEATHER_CLIENT_FRAG_TAG);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (weatherClientFragment == null) {
            weatherClientFragment = new WeatherClientFragment();
            ft.add(weatherClientFragment, WEATHER_CLIENT_FRAG_TAG);
        }
        ft.commit();
        fragmentManager.executePendingTransactions();
    } 
    
	public void retrieveGPSInfo(){
		final Context context = this;
		LocationManager manager = (LocationManager)getSystemService(Context.LOCATION_SERVICE );
		if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
			location = RexItLocation.getRexItCurrentLocation(this);
			retrieveWeatherInfo();
		}else{
			 new AlertDialog.Builder(this)
			    .setTitle("Enable GPS?")
			    .setMessage("Your GPS needs to be enabled in order to find your current location.")
			    .setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			        	Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			        	isGPSReturned = true;
						startActivity(intent);
			        }
			     })
			    .setNegativeButton("Use last known location", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			        	location = SPHelper.getLocationSP(context);
			    		retrieveWeatherInfo();
			        }
			     })
			    .setIcon(android.R.drawable.ic_dialog_alert).show(); 
		 }
	}
	
	private void retrieveWeatherInfo(){
		final Context context = this;
		if (isConnected(this)) {
			Log.d(LOG_TAG, "download weather 1");
			weatherClientFragment.getWeatherDetails(location[0], location[1]);
	        FragmentAttractionsList attractionListFragment = (FragmentAttractionsList) fragmentManager.findFragmentByTag(ATTRACTION_LIST_FRAG_TAG);
			if (attractionListFragment != null) {
				attractionListFragment.setIsLoading(true);
			}
		}
		else {
			new AlertDialog.Builder(this)
		    .setTitle("Enable Internet connection?")
		    .setMessage("You need to be connected to the Internet in order to download weather information.")
		    .setPositiveButton("Enable Internet connection", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		        	Intent intent = new Intent(Settings.ACTION_SETTINGS);
		        	isNetworkReturned = true;
					startActivity(intent);
		        }
		     })
		    .setNegativeButton("Use last known weather conditions", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		        	Log.d(LOG_TAG, "sp weather 1");
		        	weather = SPHelper.getWeatherSP(context);
		        	displayResults();
		        }
		     })
		    .setIcon(android.R.drawable.ic_dialog_alert).show(); 
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.d(LOG_TAG, "onResume()");
		if(this.isGPSReturned){
			Log.d(LOG_TAG, "isGPSReturned");
			this.isGPSReturned = false;
			final LocationManager manager = (LocationManager)getSystemService(Context.LOCATION_SERVICE );
	   		 if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
	   			 location = RexItLocation.getRexItCurrentLocation(this);
	   		 }else{
	   			location = SPHelper.getLocationSP(this);
	   		 }
	 		retrieveWeatherInfo();
		}
		
		if(this.isNetworkReturned){
			Log.d(LOG_TAG, "isNetworkReturned");
			this.isNetworkReturned = false;
			if (isConnected(this)) {
        		Log.d(LOG_TAG, "download weather 2");
    			weatherClientFragment.getWeatherDetails(location[0], location[1]);
    		} else {
    			Log.d(LOG_TAG, "sp weather 2");
    			weather = SPHelper.getWeatherSP(this);
    			displayResults();
    		}
        	FragmentAttractionsList attractionListFragment = (FragmentAttractionsList) fragmentManager.findFragmentByTag(ATTRACTION_LIST_FRAG_TAG);
 			if (attractionListFragment != null) {
 				attractionListFragment.setIsLoading(true);
 			}
		}
	}
    
    @Override
    public void onWeatherApiResponse(JSONObject json_object) {
        weather = JSONParser.parseWeatherListJSON(json_object);
        SPHelper.setWeatherSP(this, weather);
        displayResults();
    }
    
    public void displayResults(){
    	Log.d(LOG_TAG, "displayResults()");
    	
        currTime = RexItDateTime.getRexItCurrentTime();        
   		location = SPHelper.getLocationSP(this);
   		weather = SPHelper.getWeatherSP(this);
    	
        ArrayList<RexItAttraction> attractions = new ArrayList<RexItAttraction>();
        int rainFall = Integer.parseInt(weather.getRain().replaceAll(" ", ""));
        if(rainFall < 1){
            attractions = ORMLiteUtil.ormLiteQueryForMatchingAttractionsSunny(this, currTime);
        } else {
            attractions = ORMLiteUtil.ormLiteQueryForMatchingAttractionsRaining(this, currTime);
        }
        
        
        for(RexItAttraction attr : attractions){	        	
        	Location locationA = new Location("point A");     
        	locationA.setLatitude(Double.parseDouble(location[0])); 
        	locationA.setLongitude(Double.parseDouble(location[1]));
        	Location locationB = new Location("point B");
        	locationB.setLatitude(attr.getLat()); 
        	locationB.setLongitude(attr.getLng());
        	float distance = locationA.distanceTo(locationB) ;
        	attr.setDistance((int)distance);
        }
        
        Collections.sort(attractions, new CustomComparitor());
        
        FragmentAttractionsList movieListFragment = (FragmentAttractionsList) fragmentManager.findFragmentByTag("FragmentAttractionsList");
		if (movieListFragment != null) {
			Log.d(LOG_TAG, "updating list ...");
			Log.d(LOG_TAG, "num attractions: " + attractions.size());
			movieListFragment.update(attractions);
		}
        
        TextView textViewCity = (TextView) findViewById(R.id.textViewCity);
        TextView textViewTime = (TextView) findViewById(R.id.textViewTime);
        TextView textViewWeather = (TextView) findViewById(R.id.textViewWeather);
        
        textViewCity.setText(location[2]);
        textViewTime.setText("Time:     " + currTime+"");
        textViewWeather.setText("Weather : " + weather.getWeather());
    }
    
    public void displayAllAttractions(){
    	
    	ArrayList<RexItAttraction> attractions = new ArrayList<RexItAttraction>();
        attractions = ORMLiteUtil.ormLiteQueryForAllAttraction(this);
        
        FragmentAttractionsList attractionListFragment = (FragmentAttractionsList) fragmentManager.findFragmentByTag("FragmentAttractionsList");
		if (attractionListFragment != null) {
			Log.d(LOG_TAG, "updating list ...");
			Log.d(LOG_TAG, "num attractions: " + attractions.size());
			attractionListFragment.update(attractions);
		}
    }
    
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		Log.d(LOG_TAG, "onPostCreate");
	}
	
	public static boolean isConnected(Context mContext){
		ConnectivityManager conMan = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
				
		NetworkInfo netInf = conMan.getActiveNetworkInfo();	
		if (netInf != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.tab1, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_exclude_context) {
			displayAllAttractions();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
