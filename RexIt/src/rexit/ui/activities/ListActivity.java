package rexit.ui.activities;

import rexit.api.weatherapi.WeatherClientFragment;
import rexit.ui.tabs.AttractionsTab;
import rexit.ui.tabs.ContextInfoTab;
import nz.ac.waikato.cms.rexit.R;
import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class ListActivity extends TabActivity {

	private final static String 	LOG_TAG = "ListActivity";
    
    // fragment manager and dynamic fragments
    public WeatherClientFragment	weatherClientFragment;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_list);
		super.onCreate(savedInstanceState);
		Log.d(LOG_TAG, "onCreate()");
		
		// create the TabHost that will contain the Tabs
        TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);
        TabSpec tab1 = tabHost.newTabSpec("First Tab");
        TabSpec tab2 = tabHost.newTabSpec("Second Tab");

        // Set the Tab name and Activity
        // that will be opened when particular Tab will be selected
        tab1.setIndicator("Attractions");
        tab1.setContent(new Intent(this,AttractionsTab.class));
        
        tab2.setIndicator("Info");
        tab2.setContent(new Intent(this,ContextInfoTab.class));
        
        /** Add the tabs  to the TabHost to display. */
        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
        
        for(int i=0;i<tabHost.getTabWidget().getChildCount();i++) 
        {
            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(Color.parseColor("#EEEEEE"));
            tv.setTextSize(16);
            tv.setTypeface(null, Typeface.NORMAL);
        } 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {		
        getMenuInflater().inflate(R.menu.list, menu);
		Log.d(LOG_TAG, "onCreateOptionsMenu()");
		return true;
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		default:
			return super.onOptionsItemSelected(item);
		}
    }
	
	
}
