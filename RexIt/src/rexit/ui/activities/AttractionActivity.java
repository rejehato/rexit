package rexit.ui.activities;

import java.util.Locale;

import rexit.api.applications.AttractionApplication;

import nz.ac.waikato.cms.rexit.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class AttractionActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_attraction);
		
		TextView title = (TextView)findViewById(R.id.textViewTitle);
		TextView times = (TextView)findViewById(R.id.textViewTimes);
		TextView location = (TextView)findViewById(R.id.textViewLocation);
		TextView weather = (TextView)findViewById(R.id.textVieWeather);
		TextView description = (TextView)findViewById(R.id.textViewDescription);
		
		title.setText(AttractionApplication.getAttraction().getName().toUpperCase(Locale.getDefault()));
		times.setText("ATTRACTION HOURS \n" 
					+ "OPEN:  " + AttractionApplication.getAttraction().getOpen() +"\n"
					+ "CLOSE: " + AttractionApplication.getAttraction().getClose()+"\n");
		location.setText("LOCATION \n" 
					+ "LAT: " + AttractionApplication.getAttraction().getLat() +"\n"
					+ "LNG: " + AttractionApplication.getAttraction().getLng() + "\n");
		weather.setText("WEATHER CONDITIONS \n" 
					+ "CAN GO IN RAIN:   " + AttractionApplication.getAttraction().getRain() +"\n");
		description.setText("DESCRIPTION \n" + AttractionApplication.getAttraction().getDescription());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.attraction, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
