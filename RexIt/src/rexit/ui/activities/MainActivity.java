/*
 * File:			nz.ac.waikato.cms.rexit.activities.MainActivity
 * 
 * Reference: 
 * Date modified:	30/04/2015
 * Author:
 */
package rexit.ui.activities;

import rexit.db.ormlite.ORMLiteUtil;
import nz.ac.waikato.cms.rexit.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

/**
 * 
 * Class: MainActivity
 * 
 * Notes: 
 */
public class MainActivity extends Activity {
	
	private final static String 	LOG_TAG 						= "MainActivity";

	/*
	 * Method: onCreate() 
	 * 
	 * Notes: At this stage, onCreate() is used to load dummy data into
	 * the database. It is also used to pull data from the database
	 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	Log.d(LOG_TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        getActionBar().setTitle("");
        setContentView(R.layout.activity_main);
        
        // On first instance, generate dummy data in the database
        ORMLiteUtil.ormLiteGenerateDummyData(this);
    }
    
    public void onClickFind(View v){
    	Intent intent = new Intent(this, ListActivity.class);
    	startActivity(intent);
    }
    
	
	/**
	 * Menu Call Back methods
	 */

	/*
	 * Method: onCreateOptionsMenu() 
	 * 
	 * Notes: 
	 */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	Log.d(LOG_TAG, "onCreateOptionsMenu()");
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /*
	 * Method: onOptionsItemSelected() 
	 * 
	 * Notes: 
	 */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	Log.d(LOG_TAG, "onOptionsItemSelected()");
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
