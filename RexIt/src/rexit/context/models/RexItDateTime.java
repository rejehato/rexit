package rexit.context.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;

public class RexItDateTime {
	
	@SuppressLint("SimpleDateFormat") 
	public static String getRexItCurrentTime(){
		DateFormat df = new SimpleDateFormat("HH:mm");
		String currTime = df.format(Calendar.getInstance().getTime());
		return currTime;
	}

}
