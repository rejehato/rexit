
package rexit.context.models;

import org.simpleframework.xml.Path;
import org.simpleframework.xml.Text;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * 
 * Class: Attraction
 * 
 * Notes: An instance of the Attraction class will represent a row in the 
 * 'rexit_application' database table. Uses the OrmLite library to interact
 * with the 'rexit.db' database.
 */
@DatabaseTable(tableName = "rexit_attraction")
public class RexItAttraction {
	
	public static final String OPEN = "open";
	public static final String CLOSE = "close";
	public static final String RAIN = "rain";
	public static final String KIDS = "kids";
	public static final String FAMILY = "family";
	public static final String ADULT = "adult";
	
	@DatabaseField(generatedId = true)
	private int id;
	
	@Text
	@Path("name")
	@DatabaseField
	private String name;
	
	@Text
	@Path("description")
	@DatabaseField
	private String description;
	
	@Text
	@Path("lat")
	@DatabaseField
	private float lat;
	
	@Text
	@Path("lng")
	@DatabaseField 
	private float lng;
	
	@Text
	@Path("open")
	@DatabaseField (columnName = OPEN)
	private String open;
	 
	@Text
	@Path("close")
	@DatabaseField (columnName = CLOSE)
	private String close;
	
	@Text
	@Path("rain")
	@DatabaseField (columnName = RAIN)
	private String rain;
	
	@Text
	@Path("kids")
	@DatabaseField (columnName = KIDS)
	private String kids;
	
	@Text
	@Path("family")
	@DatabaseField (columnName = FAMILY)
	private String family;
	
	@Text
	@Path("adult")
	@DatabaseField (columnName = ADULT)
	private String adult;
	
	private int distance = 0;

	public RexItAttraction(){
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getLat() {
		return lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}

	public float getLng() {
		return lng;
	}

	public void setLng(float lng) {
		this.lng = lng;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getClose() {
		return close;
	}

	public void setClose(String close) {
		this.close = close;
	}

	public String getRain() {
		return rain;
	}

	public void setRain(String rain) {
		this.rain = rain;
	}

	public String getKids() {
		return kids;
	}

	public void setKids(String kids) {
		this.kids = kids;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getAdults() {
		return adult;
	}

	public void setAdults(String adults) {
		this.adult = adults;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

}
