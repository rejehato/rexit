package rexit.context.models;


/**
 * Created by rc115 on 11/04/15.
 */
public class RexItWeather {

    private String				wind;
    private String				weather;
    private String	    		temp_c;
    private String              rain;

    public RexItWeather() {

    }

	public String getWind() {
		return wind;
	}

	public void setWind(String wind) {
		this.wind = wind;
	}

	public String getWeather() {
		return weather;
	}

	public void setWeather(String weather) {
		this.weather = weather;
	}

	public String getTemp_c() {
		return temp_c;
	}

	public void setTemp_c(String temp_c) {
		this.temp_c = temp_c;
	}

	public String getRain() {
		return rain;
	}

	public void setRain(String rain) {
		this.rain = rain;
	}
    
    

}
