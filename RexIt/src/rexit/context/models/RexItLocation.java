package rexit.context.models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import rexit.util.util.SPHelper;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

public class RexItLocation {

	private static final String LOG_TAG = "RexItLocation";

	public static String[] getRexItCurrentLocation(Context context) {

		// Find location providers
		Location location = null;
		LocationManager lm = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		List<String> providers = lm.getProviders(true);

		// Choose best provider
		for (int i = providers.size() - 1; i >= 0; i--) {
			location = lm.getLastKnownLocation(providers.get(i));
			if (location != null)
				break;
		}

		// If something goes wrong with gps data, return last known location
		if (location == null)
			return SPHelper.getLocationSP(context);

		// Use lat and long to find city name
		String cityName = "";
		Geocoder gcd = new Geocoder(context, Locale.getDefault());
		List<Address> addresses = new ArrayList<Address>();
		try {
			addresses = gcd.getFromLocation(location.getLatitude(),
					location.getLongitude(), 1);
			if (addresses.size() > 0) {
				System.out.println(addresses.get(0).getLocality());
				cityName = addresses.get(0).getLocality();
			}
		} catch (IOException e) {
		}
		if (addresses.size() > 0) {
			Log.d(LOG_TAG, addresses.get(0).getLocality());
			cityName = addresses.get(0).getLocality();
		}

		// Return gps location data
		String[] locationArr = { location.getLatitude() + "",
				location.getLongitude() + "", cityName };
		SPHelper.setLocationSP(context, locationArr);
		return locationArr;
	}
}
