package rexit.xml.simplexml;

import java.io.IOException;
import java.io.InputStream;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

public class SimpleXmlParser {
	
	private static final String LOG_TAG = "SimpleXmlParser";
	
	private SimpleXmlParser() {
	}
	
	public static ResponseAttractions serializeXML(Context context) {
		AssetManager assManager = context.getAssets();
		InputStream is = null;
		try {
			is = assManager.open("dummy_data.xml");		
			
			Serializer serializer = new Persister();
			ResponseAttractions result = serializer.read(ResponseAttractions.class, is);
			return result;
		} catch (Exception e) {
			Log.e(LOG_TAG, e.getMessage().toString());
			throw new RuntimeException(e);
		} finally {
			closeInputStream(is);
		}
	}
	
	private static void closeInputStream(InputStream is){
		if (is != null) {
			try {
				is.close();
			} catch (IOException e) {
				Log.e(LOG_TAG, e.getMessage());
				throw new RuntimeException(e);
			}
		}
	}
} 
