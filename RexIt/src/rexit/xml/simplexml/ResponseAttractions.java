package rexit.xml.simplexml;

import java.util.Collection;


import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import rexit.context.models.RexItAttraction;

@Root(name="dummy")
public class ResponseAttractions {
	
	@Path("attractions")
	@ElementList(inline=true, entry="attraction")
	private Collection<RexItAttraction> attractions;
	
	public ResponseAttractions(){}

	public Collection<RexItAttraction> getAttractions() {
		return attractions;
	}

	public void setAttractions(Collection<RexItAttraction> attractions) {
		this.attractions = attractions;
	}

}
