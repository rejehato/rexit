package rexit.api.weatherapi;


import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by rc115 on 11/04/15.
 */
public class WeatherClientFragment extends Fragment {
    private static final String		TAG						= "WeatherClientFragment";

    public static final String      API_BASE_STRING         = "http://api.wunderground.com/api/";
    public static final String      AUTH_API_KEY            = "7f5d4d82f509e15f";
    public static final String      CONDITION_LOOKUP              = "/conditions/q/";

    public String                   longitude               = "";
    public String                   latitude                = "";
    public static final String      ADD_JSON                = ".json";

    private RequestQueue			requestQueue			= null;

    private WeatherResponseListener	weatherResponseListener;

    public WeatherClientFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach()");
        super.onAttach(activity);
        try {
            weatherResponseListener = (WeatherResponseListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement WeatherResponseListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate(Bundle)");
        super.onCreate(savedInstanceState);
        requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        setRetainInstance(true);
    }

    public void cancelAllRequests() {
        requestQueue.cancelAll(this);

    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
        cancelAllRequests();
    }

    public void getWeatherDetails(String lat, String lon) {
String holdUrl = API_BASE_STRING + AUTH_API_KEY + CONDITION_LOOKUP + lat + "," + lon + ADD_JSON;
final String requestUrl = holdUrl;
System.err.println(requestUrl);
JsonObjectRequest request = new JsonObjectRequest(Method.GET, requestUrl, null, new Listener<JSONObject>() {
		public void onResponse(JSONObject json_object) {
			weatherResponseListener.onWeatherApiResponse(json_object);
		}
	}, new ErrorListener() {
		public void onErrorResponse(VolleyError error) {
			weatherResponseListener.onWeatherApiResponse(null);
		}
	});
	requestQueue.add(request);
}

}
