package rexit.api.weatherapi;


import org.json.JSONException;
import org.json.JSONObject;

import rexit.context.models.RexItWeather;

import android.util.Log;

/**
 * Created by rc115 on 11/04/15.
 */
public class JSONParser {
    private static final String	TAG	= "JSONParser";

    public static RexItWeather parseWeatherJSON(JSONObject weather) {
        RexItWeather w = new RexItWeather();
        w.setWind(weather.optString("wind_string"));
        w.setWeather(weather.optString("weather"));
        w.setTemp_c(weather.optString("temp_c"));
        w.setRain(weather.optString("precip_1hr_metric"));
        return w;
    }

    public static RexItWeather parseWeatherListJSON(JSONObject json) {
        try {
            Log.d(TAG, json.toString(2));
            JSONObject obj = json.getJSONObject("current_observation");
            return parseWeatherJSON(obj);
        } catch (JSONException e) {
            Log.d(TAG, "JSONException");
            e.printStackTrace();
            return null;
        }catch(Exception e){
            RexItWeather w = new RexItWeather();
            w.setWind("Network error");
            w.setWeather("Please make sure there is Internet connection and try again");
            return w;
        }
    }
}
