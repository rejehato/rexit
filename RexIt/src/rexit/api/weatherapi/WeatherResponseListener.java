package rexit.api.weatherapi;

import org.json.JSONObject;

/**
 * Created by rc115 on 11/04/15.
 */
public interface WeatherResponseListener {

    public void onWeatherApiResponse(JSONObject json_object);

}
