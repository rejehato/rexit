package rexit.api.applications;

import rexit.context.models.RexItAttraction;
import android.app.Application;

public class AttractionApplication extends Application{
	
	private static RexItAttraction attraction;

	public static RexItAttraction getAttraction() {
		return attraction;
	}

	public static void setAttraction(RexItAttraction attraction) {
		AttractionApplication.attraction = attraction;
	}

}
