package rexit.util.util;

import java.util.Comparator;

import rexit.context.models.RexItAttraction;


public class CustomComparitor implements Comparator<RexItAttraction> {
	   
	@Override
    public int compare(RexItAttraction p1, RexItAttraction p2) {
		return ((Integer)p1.getDistance()).compareTo(p2.getDistance());
	}
}
