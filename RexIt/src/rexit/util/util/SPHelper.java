package rexit.util.util;

import rexit.context.models.RexItWeather;
import android.content.Context;
import android.content.SharedPreferences;

public class SPHelper {

	private static final String PACKAGE = "nz.ac.waikato.cms.rexit";
	private static final String LATITUDE = "latitude";
	private static final String LONGITUDE = "longitude";
	private static final String CITY = "city";

	private static final String WIND = "wind";
	private static final String WEATHER = "weather";
	private static final String TEMP = "temp";
	private static final String RAIN = "rain";

	private static final String DEF_LATITUDE = "-37.781536";
	private static final String DEF_LONGITUDE = "175.317171";
	private static final String DEF_CITY = "Hamilton";

	private static final String DEF_WIND = "From the SW at 3.8 MPH Gusting to 4.5 MPH";
	private static final String DEF_WEATHER = "Clear";
	private static final String DEF_TEMP = "12.6";
	private static final String DEF_RAIN = "0";

	public static String[] getLocationSP(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PACKAGE,
				Context.MODE_PRIVATE);
		String lat = prefs.getString(LATITUDE, DEF_LATITUDE);
		String lng = prefs.getString(LONGITUDE, DEF_LONGITUDE);
		String city = prefs.getString(CITY, DEF_CITY);
		return new String[] { lat, lng, city };
	}

	public static void setLocationSP(Context context, String[] location) {
		SharedPreferences prefs = context.getSharedPreferences(PACKAGE,
				Context.MODE_PRIVATE);
		prefs.edit().putString(LATITUDE, location[0]);
		prefs.edit().putString(LONGITUDE, location[1]);
		prefs.edit().putString(CITY, location[2]);
	}

	public static RexItWeather getWeatherSP(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PACKAGE,
				Context.MODE_PRIVATE);
		RexItWeather weather = new RexItWeather();
		weather.setRain(prefs.getString(RAIN, DEF_RAIN));
		weather.setTemp_c(prefs.getString(TEMP, DEF_TEMP));
		weather.setWeather(prefs.getString(WEATHER, DEF_WEATHER));
		weather.setWind(prefs.getString(WIND, DEF_WIND));
		return weather;
	}

	public static void setWeatherSP(Context context, RexItWeather weather) {
		SharedPreferences prefs = context.getSharedPreferences(PACKAGE,
				Context.MODE_PRIVATE);
		prefs.edit().putString(RAIN, weather.getRain());
		prefs.edit().putString(TEMP, weather.getTemp_c());
		prefs.edit().putString(WEATHER, weather.getWeather());
		prefs.edit().putString(WIND, weather.getWind());
	}

}
