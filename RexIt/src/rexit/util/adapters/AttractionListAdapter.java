
package rexit.util.adapters;

import java.util.ArrayList;

import rexit.context.models.RexItAttraction;

import nz.ac.waikato.cms.rexit.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

final class ViewHolder {
	TextView nameView;
	TextView descriptionView;
	TextView distanceView;
}

public class AttractionListAdapter extends ArrayAdapter<RexItAttraction> {

	// Declare variables for adapter
    private final Context context;
    private final ArrayList<RexItAttraction> docList;
    
    /* Class constructor */
    public AttractionListAdapter(Context context, ArrayList<RexItAttraction> itemsArrayList) {
        super(context, R.layout.list_item_attraction, itemsArrayList);
        this.context 			= context;
        this.docList 			= itemsArrayList;
    }
    
    public AttractionListAdapter getListAdapter(){
    	return this;
    }

	@SuppressLint("InflateParams") 
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder;
        if (convertView == null || convertView instanceof FrameLayout) {
			holder = new ViewHolder();
			// Create view from inflater
			convertView = inflater.inflate(R.layout.list_item_attraction, parent, false);
			// Get the text view from the group row View
			holder.nameView = (TextView) convertView.findViewById(R.id.tv_name);
			holder.descriptionView = (TextView) convertView.findViewById(R.id.tv_description);
			holder.distanceView = (TextView) convertView.findViewById(R.id.tv_distance);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.nameView.setText(docList.get(position).getName());
		holder.distanceView.setText(docList.get(position).getDistance()+" m away");
		holder.descriptionView.setText(docList.get(position).getDescription());
		
		// return rowView
		return convertView;
    }
}
